-------------------------------------------------------
--! @file
--! @author Julian Mendez <julian.mendez@cern.ch> (CERN - EP-ESE-BE)
--! @version 2.0
--! @brief lpGBT-FPGA IP - Common package
-------------------------------------------------------

-- IEEE VHDL standard LIBRARY:
LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

PACKAGE lpgbtfpga_package is

   --=============================== Constant Declarations ===============================--
	CONSTANT FEC5						  : integer := 1;
	CONSTANT FEC12						  : integer := 2;
	CONSTANT DATARATE_5G12				  : integer := 1;
	CONSTANT DATARATE_10G24				  : integer := 2;
	CONSTANT PCS         				  : integer := 0;
	CONSTANT PMA         				  : integer := 1;
   --=====================================================================================--


	type type_gthx_reset is record
		gbtTx : std_logic;
		gbtRx : std_logic;
		mgtTx : std_logic;
		mgtRx : std_logic;
		drp   : std_logic;
	end record;

	-- type type_lpgbt_ctrl is record

	-- end record;

	type type_gthx_eyescan is record
		--- # RX Initialization and Reset Ports
		eyeScanRst	: std_logic;
		rxUserRdy	: std_logic;
		--- # RX Margin Analysis Ports
		eyeScanTrg	: std_logic;
	end record;

	type type_gthx_drp_to_gth is record
		--- # DRP Ports -------------
		drpAddr         : std_logic_vector(8 downto 0); -- => "000000000",
		drpClk          : std_logic; -- => clk_40MHz_i,
		drpDi           : std_logic_vector(15 downto 0); -- => x"0000",
		drpEn           : std_logic; -- => '0',
		drpWe           : std_logic; -- => '0',
		--- # RX Equalizer Ports -------------
		rxdfelpmreset   : std_logic; -- => '0',
		rxmonitorsel    : std_logic_vector(1 downto 0); -- => "00",
		--- # RX Initialization and Reset Ports -------------
		rxpmareset      : std_logic; -- => '0',
		--- #  TX Initialization and Reset Ports -------------
		txuserrdy		: std_logic; -- => '0',
	end record;

	type type_gthx_drp_from_gth is record
		--- # DRP Ports -------------
		drpDo			: std_logic_vector(15 downto 0); --=> open,
		drpRdy			: std_logic; --=> open,
		--- # Digital Monitor Ports -------------
		dMonitorOut		: std_logic_vector(7 downto 0); --=> open,
		--- # RX Buffer Bypass Ports -------------
		rxPhMonitor		: std_logic; --=> open,
		rxPhSlipMonitor	: std_logic; --=> open,
		--- # RX Equalizer Ports -------------
		rxMonitorOut	: std_logic_vector(6 downto 0); --=> open,
		--- #  RX Fabric Output Control Ports -------------
		rxOutClk		: std_logic; --=> mgtRxUsrClk_nobuf(link),
		rxOutClkFabric	: std_logic; --=> open,
		--- #  TX Fabric Clock Output Control Ports -------------
		txOutClk		: std_logic; --=> mgtTxUsrClk_nobuf(link),
		txOutClkFabric	: std_logic; --=> open,
		txOutClkPcs		: std_logic; --=> open,
	end record;

	type type_gthx_equaliser_cnfg is record
		postCursor   : std_logic_vector(4 downto 0); -- => "01010", -- 0.00 dB
		preCursor    : std_logic_vector(4 downto 0); -- => "00011", -- 0.68 dB
		diffCtrl     : std_logic_vector(3 downto 0); -- => "0100", -- 543 mVppd
	end record;

	constant DO_EYESCAN : boolean := true;

	type type_gthx_reset_array is array (natural range <>) of type_gthx_reset;
	type type_gthx_eyescan_array is array (natural range <>) of type_gthx_eyescan;
	type type_gthx_drp_to_gth_array is array (natural range <>) of type_gthx_drp_to_gth;
	type type_gthx_drp_from_gth_array is array (natural range <>) of type_gthx_drp_from_gth;
	type type_gthx_equaliser_cnfg_array is array (natural range <>) of type_gthx_equaliser_cnfg;
	type lpgbt_reset_fsm_state_type is (Idle, StopQPLLReset, WaitQPLLLock, StopTxGbt, StopTxMgt, StopRxMgt, StopRxGbt);
	
END lpgbtfpga_package;